<?php
namespace App\Controller;

use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Form\AjoutAuteurType;
use App\Repository\TagRepository;
use App\Repository\AuteurRepository;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/*class Controller extends AbstractController
{
    #[Route('/', name: 'display_data')]
    public function index(AuteurRepository $auteurRepo, ArticleRepository $articleRepo, TagRepository $tagRepo, Request $request): Response
    {
        $auteurs = $auteurRepo->findAll();
        $articles = $articleRepo->findAll();
        $tags = $tagRepo->findAll();


        return $this->render('test.html.twig', [
            'auteurs' => $auteurs,
            'articles' => $articles,
            'tags' => $tags,
        ]);
    }
}*/


use Doctrine\ORM\EntityManagerInterface;

class Controller extends AbstractController
{
    #[Route('/', name: 'display_data')]
    public function index(Request $request, EntityManagerInterface $entityManager, AuteurRepository $auteurRepo, ArticleRepository $articleRepo, TagRepository $tagRepo): Response
    {
        $auteur = new Auteur();
        $form = $this->createForm(AjoutAuteurType::class, $auteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($auteur);
            $entityManager->flush();

            return $this->redirectToRoute('display_data');
        }

        $auteurs = $auteurRepo->findAll();
        $articles = $articleRepo->findAll();
        $tags = $tagRepo->findAll();

        return $this->render('test.html.twig', [
            'form' => $form->createView(),
            'auteurs' => $auteurs,
            'articles' => $articles,
            'tags' => $tags,
        ]);
    }

    #[Route('/delete-auteur/{id}', name: 'delete_auteur')]
public function deleteAuteur(EntityManagerInterface $entityManager, AuteurRepository $auteurRepo, int $id): Response
{
    $auteur = $auteurRepo->find($id);
    if ($auteur) {
        $entityManager->remove($auteur);
        $entityManager->flush();
    }

    return $this->redirectToRoute('display_data');
}

#[Route('/edit-auteur/{id}', name: 'edit_auteur')]
public function editAuteur(Request $request, EntityManagerInterface $entityManager, AuteurRepository $auteurRepo, int $id): Response
{
    $auteur = $auteurRepo->find($id);
    if (!$auteur) {
        throw $this->createNotFoundException('Auteur non trouvé.');
    }

    $form = $this->createForm(AjoutAuteurType::class, $auteur);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $entityManager->flush();
        return $this->redirectToRoute('display_data');
    }

    return $this->render('edit_auteur.html.twig', [
        'form' => $form->createView(),
    ]);
}

}
