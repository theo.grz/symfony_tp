<?php
namespace App\Controller;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Response;



class UserController9 extends AbstractController
{
    #[Route('/admin/user-list', name: 'admin_user_list')]
    public function userList(UserRepository $userRepository, AuthorizationCheckerInterface $authChecker): Response
    {
        if (!$authChecker->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        
        $users = $userRepository->findAll();

        return $this->render('admin/user_list.html.twig', [
            'users' => $users,
        ]);
    }
    
    #[Route('/edit-user/{id}', name: 'edit_user')]
public function editUser(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordEncoder, UserRepository $userRepository, int $id): Response
{
    $user = $userRepository->find($id);
    if (!$user) {
        throw $this->createNotFoundException();
    }

    $form = $this->createForm(UserType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $newPassword = $form->get('password')->getData();
        if (!empty($newPassword)) {
            $user->setPassword(
                $passwordEncoder->hashPassword(
                    $user,
                    $newPassword
                )
            );
        }
    
        $entityManager->flush();

        return $this->redirectToRoute('display_data');
    }

    return $this->render('user/edit.html.twig', [
        
        'userForm' => $form,
    ]);
}
}
